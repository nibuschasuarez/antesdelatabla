import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaComponent } from './components/agenda/agenda.component';
import { VerComponent } from './components/agenda/ver/ver.component';
import { DatosComponent } from './components/datos/datos.component';
import { EstudiosComponent } from './components/estudios/estudios.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { HabilidadesComponent } from './components/habilidades/habilidades.component';
import { InicioComponent } from './components/inicio/inicio.component';

const routes: Routes = [
  { path:'informacion', component: DatosComponent},
  { path: 'ver/:id',component:VerComponent},
  { path:'agenda',component:AgendaComponent},
  { path:'estudios', component:EstudiosComponent},
  { path: 'formulario', component:FormularioComponent},
  {path:'habilidades', component:HabilidadesComponent},
  { path:'inicio', component:InicioComponent},
  {path:'**', pathMatch:'full', redirectTo:'inicio'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
