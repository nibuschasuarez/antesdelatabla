import { identifierName } from '@angular/compiler';
import { Component, OnInit, ɵsetAllowDuplicateNgModuleIdsForTest } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { citaI, CitasService } from 'src/app/services/citas.service';
import { __param } from 'tslib';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit {
  form!: FormGroup;
  citas!: citaI;
  termino!: string; 

  constructor( private activateRound: ActivatedRoute,
               private citaService:CitasService,
                private router: Router,
                private fb: FormBuilder,
                
              ) {

    this.activateRound.params.subscribe(params => {
      
      const id = params ['id'];
      console.log(id,'');
      this.form = this.fb.group({
        nombre: ['', [Validators.required, Validators.minLength(3)]],
        apellido: ['', [Validators.required, Validators.minLength(3)]],
        correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
        telefono: ['', [Validators.required, Validators.minLength(8)]],
        comentario: ['', [Validators.required, Validators.minLength(7)]],
        lugar: ['', Validators.required],
        hora: ['', Validators.required],
        fecha: ['', Validators.required]
      });
      
    if(id !== 'nuevo'){
      const cita = this.citaService.buscarcita(id);
      console.log(cita);
      this.termino = cita.nombre
      
    
   
    if(Object.keys(cita).length === 0){
    
    }
    this.form.patchValue({
      nombre: cita.nombre,
      apellido: cita.apellido,
      correo: cita.correo,
      telefono: cita.telefono,
      comentario: cita.comentario,
      hora:cita.hora,
      fecha:cita.fecha

    })

 }
})


   }


  ngOnInit(): void {

    this.activateRound.params.subscribe(params => { //subscribe obtiene una variable
      console.log(params['termino']);
      this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable 
      // this.router.navigate(['/ver', this.termino])
      this.citas= this.citaService.buscarHeroes(this.termino);

      console.log(this.citas,'citaencontrada');
      
    })
  }

  
  buscar(termino: string):void{

    const encontrado = this.citaService.buscarHeroes(this.termino)
    console.log(encontrado);
    
    console.log(termino, 'buscaren Agenda');
    
    this.router.navigate(['/ver', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
 
  }

  


}
