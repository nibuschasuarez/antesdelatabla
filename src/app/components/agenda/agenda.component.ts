import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CitasService } from 'src/app/services/citas.service';



@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  [x: string]: any;

  events: calendarI[] = [
    { title: 'Present', date: '2022-06-01', color: '#0000ff' },
    { title: 'absent', date: '2022-06-02', color: '#ff0000' }
  ];

  listacitas!: citaI[];
  mensaje!: string;
  agradecimiento: string ='¡¡¡Su cita ha sido agendada con éxito!!!'
  EJnombre!: string;
  dataSource !: MatTableDataSource<any>;

  formulario!: FormGroup;
  encontrado! : citaI
  constructor(private fb: FormBuilder,
    private router: Router,
    private _citaservice: CitasService) { 

    this.crearFormulario()
    // console.log(this.datos);
    
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      lugar: ['', Validators.required],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
    });
  }

  
  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.formulario.get('apellido')?.invalid && this.formulario.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched
  }

  get telefonoNoValido(){
    return this.formulario.get('telefono')?.invalid && this.formulario.get('telefono')?.touched
  }

  


  ngOnInit(): void {
  }

  guardar(): void{
        
        this.añadirInfo();
        this.mensaje = this.agradecimiento

    
        this.formulario.reset()
        setTimeout(() => {
          this.mensaje = ''
        }, 3000);
      }
    
  añadirInfo(){
    const user: citaI = {
      nombre: this.formulario.value.nombre,
      apellido: this.formulario.value.apellido,
      telefono: this.formulario.value.telefono,
      correo: this.formulario.value.correo,
      comentario: this.formulario.value.comentario,
      fecha: this.formulario.value.fecha,
      hora: this.formulario.value.hora
    }
    console.log(user);
    
    const calendar: calendarI  = {
      title: this.formulario.value.nombre,
      date: this.formulario.value.fecha,
      color: '#666'
    }
    // this.agregarUsuario(calendar)
    this._citaservice.agregarTabla(user)
  }
    
  agregarUsuario(cita: calendarI): void {
    this.events.unshift(cita);
    console.log(this.events, 'alba');
    // this.calendarOptions.events = this.events

  }


  // eliminarCitaU(cita: string){
  //   console.log('intento eliminar');
    
  //   this.eliminarcita(cita);
  //   this.cargarCita();

  // }
 eliminarCita(cita: citaI){
  this._citaservice.eliminarcita(cita)
  
  console.log('intento eliminar');
  console.log(cita, 'elimiar');
  const response = confirm('Estás seguro que deseas eliminar la cita?')

  if(response){
    for(let i = 0; i < this.listacitas.length; i ++){
      if(cita == this.listacitas[i]){
        console.log('eli');
        
        this.listacitas.splice(i, 1);
        localStorage.setItem('Citas', JSON.stringify(this.listacitas))
      }
    }
    
  }

    this.listacitas = this.listacitas.filter(data => {
        return data.nombre!== cita.nombre
    })
 }

  getcita(): calendarI[]{
    return this.events.slice()
    
  }

  obtenerLocalStorage(){
    let citaLista = JSON.parse(localStorage.getItem("Citas") || 'asd');
    console.log(citaLista);
    
    this.listacitas = citaLista
    console.log(citaLista, 'obtener LocalStorage');
  }

 
  verCita(cita: string){
    console.log(cita);
    
    this.router.navigate(['/ver/' , cita])

  }

  modificarCita(cita: string){
    console.log(cita, 'Modificacion');
    this.router.navigate(['/agenda/editar', cita])

  }
  MensajeEnviado(): void{
    this.formulario.reset()

    this.mensaje = this.agradecimiento
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);
  }

  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/crear-usuario', usuario]);
  }

 
cargarCitas() {
    
    this.dataSource = new MatTableDataSource(this.listacitas)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
    
  buscarHeroe(termino: string): void{
  this.encontrado = this._citaservice.buscarHeroes (termino)
    console.log(this.encontrado);
    
  }

  enviarencontrado(){
    return 
  }

  
  
}

interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}

interface calendarI {
  title: string,
  date: string,
  color: string
}